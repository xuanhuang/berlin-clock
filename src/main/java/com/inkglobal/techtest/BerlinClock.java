package com.inkglobal.techtest;

import java.util.Calendar;

public class BerlinClock {

    public String getClock(Calendar calendar){
        int second = calendar.get(Calendar.SECOND);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        return getTopLamp(second) + " " + getTopTwoRows(hour) + " " + getBottomTwoRows(minute);
    }

    /**
     * From description 'On the top of the clock there is a yellow lamp that blinks on/off every two seconds.'
     * I assumed that the lamp is 'ON' for 1 second, and 'OFF' for 1 second, starting from being 'ON' on 0 second.
    */
    public String getTopLamp(int second){
        if((second % 2) == 0) {
            return "Y";
        }else{
            return "O";
        }
    }

    public String getTopTwoRows(int hour){
        int firstRowCount = hour/5;
        int secondRowCount = hour - (firstRowCount * 5);
        return printHourRow(firstRowCount) + " " + printHourRow(secondRowCount);
    }

    public String getBottomTwoRows(int minutes){
        int firstRow = minutes/5;
        int secondRow = minutes - firstRow * 5;
        return printFirstMinuteRow(firstRow) + " " + printSecondMinuteRow(secondRow);
    }

    public String printHourRow(int onLampCount){
        String hourRow = "";
        for (int i = 0; i < 4; i++) {
            if(i < onLampCount){
                hourRow += "R";
            }
            else{
                hourRow += "O";
            }
        }
        return hourRow;
    }

    public String printFirstMinuteRow(int count){
        String minuteRow = "";
        for (int i = 0; i < 11; i++) {
            if(i < count){
                if(i == 2 || i == 5 || i == 8){
                    minuteRow += "R";
                }else{
                    minuteRow += "Y";
                }

            }else{
                minuteRow += "O";
            }
        }
        return minuteRow;
    }

    public String printSecondMinuteRow(int count){
        String minuteRow = "";
        for (int i = 0; i < 4; i++) {
            if(i < count){
                minuteRow += "Y";
            }
            else{
                minuteRow += "O";
            }
        }
        return minuteRow;
    }
}
