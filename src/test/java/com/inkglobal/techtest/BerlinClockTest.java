package com.inkglobal.techtest;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class BerlinClockTest {
    public BerlinClock testObj;
    public Calendar cal;

    @Before
    public void setUp(){
        testObj = new BerlinClock();
        cal = Calendar.getInstance();
    }

    @Test
    public void testCase1(){
        //setup
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        String expectedClock = "Y OOOO OOOO OOOOOOOOOOO OOOO";

        //act
        String clock = testObj.getClock(cal);

        //assert
        assertEquals(expectedClock, clock);
    }

    @Test
    public void testCase2(){
        //setup
        cal.set(Calendar.HOUR_OF_DAY, 13);
        cal.set(Calendar.MINUTE, 17);
        cal.set(Calendar.SECOND, 1);
        String expectedClock = "O RROO RRRO YYROOOOOOOO YYOO";

        //act
        String clock = testObj.getClock(cal);

        //assert
        assertEquals(expectedClock, clock);
    }


    @Test
    public void testCase3(){
        //setup
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        String expectedClock = "O RRRR RRRO YYRYYRYYRYY YYYY";

        //act
        String clock = testObj.getClock(cal);

        //assert
        assertEquals(expectedClock, clock);
    }

    /**
     * I think in this test case, 24:00:00 should be the same as 00:00:00
     * Therefore the given expected result is not correct, and it contradicts with test case 1.
     *
     */
    @Test
    public void testCase4(){
        //setup
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        String expectedClock = "Y RRRR RRRR OOOOOOOOOOO OOOO";
        String actualClock = "Y OOOO OOOO OOOOOOOOOOO OOOO";

        //act
        String clock = testObj.getClock(cal);

        //assert
        assertEquals(actualClock, clock);
    }
}
